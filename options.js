// Saves options to chrome.storage
function save_options() {
  var assigneeWarning = document.getElementById('assigneeWarning').checked;
  var commentWarning = document.getElementById('commentWarning').checked;
  var assigneeWarningContent = document.getElementById('assigneeWarningContent').value;
  var commentWarningContent = document.getElementById('commentWarningContent').value
  var usernameValue = document.getElementById('username').value;
  chrome.storage.local.set({
    enableAssigneeWarning: assigneeWarning,
    enableCommentWarning: commentWarning,
    assigneeWarningText: assigneeWarningContent,
    commentWarningText: commentWarningContent,
    username: usernameValue
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

function toggle_textarea_visibility(value, element) {
    if (!value) {
        element.style = "display:none";
    }
    else {
        element.style = "display:block";
    }
}
// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.local.get({
    enableAssigneeWarning: true,
    enableCommentWarning: true,
    assigneeWarningText: '<p style="color:red">Zkontroluj:<br>Stav tasku<br>Planning status<br>Assignee</p>',
    commentWarningText: '<p style="color:red;font-weight:bold">Zkontroluj:<br>Stav tasku<br>Planning status<br>Assignee<br>Přístupnost komentáře (puxdesign-users)</p>',
    username: ''
  }, function(items) {
    var assigneeWarningEnabledElement = document.getElementById('assigneeWarning');
    var commentWarningEnabledElement = document.getElementById('commentWarning');
    var assigneeWarningContentElement = document.getElementById('assigneeWarningContent');
    var commentWarningContentElement = document.getElementById('commentWarningContent');
    var usernameElement = document.getElementById('username')

    assigneeWarningEnabledElement.checked = items.enableAssigneeWarning;
    commentWarningEnabledElement.checked = items.enableCommentWarning;
    assigneeWarningContentElement.value = items.assigneeWarningText;
    commentWarningContentElement.value = items.commentWarningText;
    usernameElement.value = items.username;

    assigneeWarningEnabledElement.addEventListener('click', function() {toggle_textarea_visibility(assigneeWarningEnabledElement.checked, assigneeWarningContentElement)});
    commentWarningEnabledElement.addEventListener('click', function() {toggle_textarea_visibility(commentWarningEnabledElement.checked, commentWarningContentElement)});

    toggle_textarea_visibility(items.enableAssigneeWarning, assigneeWarningContentElement);
    toggle_textarea_visibility(items.enableCommentWarning, commentWarningContentElement);
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',
    save_options);