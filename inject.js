(function() {
	var boldWarning = "<p style='color:red;font-weight:bold'> Zkontroluj:<br>Stav tasku <br>Tým<br>Planning Status<br>Přístupnost komentáře (puxdesign-users)</p>";
	var simpleWarning = "<p style='color:red;'> Zkontroluj:<br>Stav tasku<br>Tým<br>Planning Status</p>";
	
	var loadedSettings = ['enableAssigneeWarning', 'enableCommentWarning', 'assigneeWarningText', 'commentWarningText', 'username']
	chrome.storage.local.get(loadedSettings, function(result) {
		var enableAssigneeWarning = result.enableAssigneeWarning;
		var enableCommentWarning = result.enableCommentWarning;
		var assigneeWarning = result.assigneeWarningText;
		var commentWarning = result.commentWarningText;
		var username = result.username;
		if (enableAssigneeWarning) {
			var emailElement = document.querySelector("span#assignee-val span.user-hover");
			var assigneeElement = document.querySelector("div#peopledetails dl dd");
			if (emailElement.getAttribute("rel") == username) {
				assigneeElement.insertAdjacentHTML("afterEnd", assigneeWarning);
			}
		}
		if (enableCommentWarning) {
			var commentForm = document.querySelector("form#issue-comment-add .form-body");
			commentForm.insertAdjacentHTML("afterEnd", commentWarning);
		}
	});
})();